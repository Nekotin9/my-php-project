<?php

function createFileWithSum(string $pathToFiles): void {
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles

    if (is_dir($pathToFiles)) {

        $file_1 = file($pathToFiles . '/1.txt');
        $file_2 = file($pathToFiles . '/2.txt');
        $array = array();
        foreach ($file_1 as $key => $value) {
            if (isset($file_2[$key]))
                $array[$key] = $value + $file_2[$key];
            else $array[$key] = $value;
        }

        $string = implode(PHP_EOL, $array);
        $string_data = file_put_contents($pathToFiles . "/3.txt", $string);
        $array = unserialize($string_data);
    }


}