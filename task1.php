<?php

function reverseString($str) {
    // Функция должна выводить последовательность символов в обратном порядке.
    // В аргумент $str может быть передана не только строка

    $str = iconv('utf-8', 'windows-1251', $str);
    $str = strrev($str);
    $str = iconv('windows-1251', 'utf-8', $str);
    return $str;
}
